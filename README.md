# zipline tutorials

This repository is aimed to provide code samples on how to use zipline as a backtesting engine.

## Setup

Installing zipline can be troublesome. Below we provide some guidelines in order to install it on macOs. 
These guidelines have mostly been taken from the book 'Trading Evolved' from Andreas F. Clenow. However some changes have been implemented
in order to make it work.

1. Install anaconda : https://www.anaconda.com/distribution/
2. Upgrade pip:
	- Open Anaconda Navigator and launch terminal base(root)
	- to find python interpreter path type: which python
	- type : your_interpreter_path -m pip install --upgrade pip
3. Open anaconda navigator
4. Go to Environments (menu on the left)
5. Create new environment (menu at the bottom). Choose Python 3.5 and name environment : env_zipline (This name is not mandatory and can be changed)
6. Select environment env_zipline and click on channels
7. Click on add, type ‘Quantopian’ (without quotes) and tap enter
8. Back on the anaconda navigator look for non installed packages on the env_zipline environment, and search for zipline.
9. Tick the box on zipline and click on the apply button. New packages will install.
10. On the env_zipline environment look for the not installed libraries and apply:
	- Matplotlib : this library can be installed from the anaconda prompt as the zipline library on step 6
	- \_ipyw_ilab_nb_ex (same as above)

We now have to modify some files from the newly installed zipline software. For this go to the section __Typo in “Patching the Framework”__
in the following link and follow the instructions: https://www.followingthetrend.com/2019/08/trading-evolved-errata-and-updates/

## Ingesting Quandl bundle on MacOs
The data necessary for the tutorials is contained on the Quandl bundle, which you have to install too.
For this you need a Quandl API key, which you can obtain by signing in here : https://www.quandl.com
Now follow these instructions:

1. Go to the anaconda navigator and go to Environments (menu on the left)
1. Click on the triangle next to env_zipline and then click on Open terminal
2. A shell prompt will open. Run the following commands on the shell:
	- export QUANDL_API_KEY=<your_quandl_api_key>
	- zipline ingest -b quandl
	
## Installing Pyfolio
You will need Pyfolio to visualize the results of your backtest on an orderly manner.
For that you just need to run the following code on the env_zipline environment:

pip install pyfolio
